#!/usr/bin/env bash

## Create MySQL User
useradd -s /usr/sbin/nologin mysql

## Create Required Directories
mkdir -p /etc/mysql/conf.d/
mkdir /var/log/mysql/
mkdir /var/run/mysqld/
mkdir -p /home/database/bin-logs/
mkdir /home/database/data/
mkdir /home/database/innodb/
mkdir /home/database/innodb-logs/
mkdir /home/database/relay-logs/

## Move Config
cp /vagrant/config/my.cnf /etc/mysql/my.cnf
cp /vagrant/config/testdbm1.cnf /etc/mysql/conf.d/testdbm1.cnf

## Fix Permissions
chown -R mysql:mysql /etc/mysql/
chown -R mysql:mysql /home/database/
chown mysql:mysql /var/log/mysql/
chown mysql:mysql /var/run/mysqld/

## Install Repo & MySQL
yum install -y http://www.percona.com/downloads/percona-release/percona-release-0.0-1.x86_64.rpm
yum install -y Percona-Server-server-55.x86_64

## Start MySQL
service mysql start

## Create UDF's
mysql -e "CREATE FUNCTION fnv1a_64 RETURNS INTEGER SONAME 'libfnv1a_udf.so'"
mysql -e "CREATE FUNCTION fnv_64 RETURNS INTEGER SONAME 'libfnv_udf.so'"
mysql -e "CREATE FUNCTION murmur_hash RETURNS INTEGER SONAME 'libmurmur_udf.so'"