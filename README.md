# README

This repo contains some useful Vagrant stuff I've been playing around with.

Assumptions:

* You have setup and installed [Vagrant](http://docs.vagrantup.com/v2/installation/index.html)
* You are not using Windows (Because CR/LF)
* You enjoy pickles
